﻿/*
  hoja 7 - unidad 1 - modulo 3 - ejercicio 3
*/

-- base de datos

DROP DATABASE IF EXISTS hoja7unidad1modulo3ejercicio3;

CREATE DATABASE IF NOT EXISTS hoja7unidad1modulo3ejercicio3;

USE hoja7unidad1modulo3ejercicio3;


-- turista
DROP TABLE IF EXISTS turista;

CREATE TABLE IF NOT EXISTS turista(
  t char(3),
  dir varchar(15),
  nombre varchar(15),
  apellidos varchar(15),
  tel varchar(15),
  PRIMARY KEY (t)
  );


-- hotel

DROP TABLE IF EXISTS hotel;

CREATE TABLE IF NOT EXISTS hotel(
  h char(3),
  nombre varchar(15),
  dir varchar(15),
  ciudad varchar(15),
  plazas int,
  tel varchar(15),
  PRIMARY KEY(h)
  );


-- reserva

DROP TABLE IF EXISTS reserva;

CREATE TABLE IF NOT EXISTS reserva(
  turista char(3),
  hotel char(3),
  fechaEntrada date,
  fechaSalida timestamp,
  pension varchar(30),
  PRIMARY KEY (turista, hotel),
  CONSTRAINT FKResrvaTurista FOREIGN KEY (turista)
  REFERENCES turista(t) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKReservaHotel FOREIGN KEY (hotel)
  REFERENCES hotel(h) ON DELETE CASCADE ON UPDATE CASCADE
  );

-- vuelo

DROP TABLE IF EXISTS vuelo;

CREATE TABLE IF NOT EXISTS vuelo(
  n char(3),
  fecha date,
  hora time,
  origen varchar(15),
  destino varchar(15),
  nturista int,
  ntotal int,
  PRIMARY KEY (n)
  );


-- toma

DROP TABLE IF EXISTS toma;
CREATE TABLE IF NOT EXISTS toma(
  turista char(3),
  vuelo char(3),
  clase varchar(15),
  PRIMARY KEY(turista, vuelo),
  CONSTRAINT FKTomaTurista FOREIGN KEY (turista)
  REFERENCES turista(t) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKTomaVuelo FOREIGN KEY (vuelo)
  REFERENCES vuelo(n) ON DELETE CASCADE ON UPDATE CASCADE
  );


-- agencia

DROP TABLE IF EXISTS agencia;
CREATE TABLE IF NOT EXISTS agencia(
  s char(3),
  direccion varchar(15),
  tel varchar(15),
  PRIMARY KEY(s)
  );


-- contrata

DROP TABLE IF EXISTS contrata;
CREATE TABLE IF NOT EXISTS contrata(
  turista char(3),
  agencia char(3),
  PRIMARY KEY(turista, agencia),
  CONSTRAINT FKContrataTurista FOREIGN KEY(turista)
  REFERENCES turista(t) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKContrataAgencia FOREIGN KEY(agencia)
  REFERENCES agencia(s) ON DELETE CASCADE ON UPDATE CASCADE
  );
